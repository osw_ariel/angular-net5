using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Dtos;
using Api.Entities;

namespace Api.Interfaces
{
    public interface IUserRepository
    {
        void Update(AppUserClass user);
        Task<bool> SaveAllAsync();
        Task<AppUserClass> GetUserByIdAsync(int id);
        Task<AppUserClass> GetUserByUserNameAsync(string userName);        
        Task<IEnumerable<AppUserClass>> GetUsersAsync();
        Task<IEnumerable<MemberDto>> GetMembersAsync();
        Task<MemberDto> GetMemberAsync(string userName);
    }
}